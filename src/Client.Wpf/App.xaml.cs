﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using Client.Wpf.ViewModels;

namespace Client.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        

        #region Overrides of Application

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            InitializeDiContainer();
        }

        private void InitializeDiContainer()
        {
            var assembly = typeof(App).Assembly;
            var builder = new ContainerBuilder();

            // Registers both modules
            builder.RegisterAssemblyModules(assembly);

            var container = builder.Build();

            Resources["ViewModelLocator"] = container.Resolve<ViewModelLocator>();
        }

        #endregion
    }
}
