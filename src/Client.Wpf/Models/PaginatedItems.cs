﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Wpf.Models
{
    class PaginatedItems<T>
    {
        public IEnumerable<T> Items { get; }
        public int TotalPages { get; }
        public int Page { get; }

        public PaginatedItems(IEnumerable<T> items, int totalPages, int page)
        {
            Items = items;
            TotalPages = totalPages;
            Page = page;
        }
    }
}
