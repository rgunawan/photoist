﻿namespace Client.Wpf.Models
{
    class PhotoInfo
    {
        public int Id { get; }
        public string FullSizeUrl { get; }
        public string ThumbnailUrl { get; }

        public PhotoInfo(int id, string fullSizeUrl, string thumbnailUrl)
        {
            Id = id;
            FullSizeUrl = fullSizeUrl;
            ThumbnailUrl = thumbnailUrl;
        }
    }
}