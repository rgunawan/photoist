﻿using System.Windows;

namespace Client.Wpf.Controls
{
    /// <summary>
    /// Interaction logic for Masthead.xaml
    /// </summary>
    public partial class Masthead
    {
        #region AppName

        public static readonly DependencyProperty AppNameProperty = DependencyProperty.Register(
            "AppName", typeof(string), typeof(Masthead), new PropertyMetadata(default(string)));

        public string AppName
        {
            get { return (string) GetValue(AppNameProperty); }
            set { SetValue(AppNameProperty, value); }
        }

        #endregion

        


        public Masthead()
        {
            InitializeComponent();
            
        }
    }
}
