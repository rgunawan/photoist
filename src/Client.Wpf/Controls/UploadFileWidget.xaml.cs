﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Client.Wpf.Helpers.Xaml;

namespace Client.Wpf.Controls
{
    /// <summary>
    /// Interaction logic for UploadFileWidget.xaml
    /// </summary>
    public partial class UploadFileWidget
    {
        #region Path

        public static readonly DependencyProperty PathProperty = DependencyProperty.Register(
            "Path", typeof(string), typeof(UploadFileWidget), new PropertyMetadata(default(string)));

        public string Path
        {
            get { return (string)GetValue(PathProperty); }
            set { SetValue(PathProperty, value); }
        }

        #endregion

        #region EditPathCommand

        public static readonly DependencyProperty EditPathCommandProperty = DependencyProperty.Register(
            "EditPathCommand", typeof(ICommand), typeof(UploadFileWidget), new PropertyMetadata(default(ICommand)));

        public ICommand EditPathCommand
        {
            get { return (ICommand)GetValue(EditPathCommandProperty); }
            set { SetValue(EditPathCommandProperty, value); }
        }

        #endregion

        #region UploadCommand

        public static readonly DependencyProperty UploadCommandProperty = DependencyProperty.Register(
            "UploadCommand", typeof(ICommand), typeof(UploadFileWidget), new PropertyMetadata(default(ICommand)));

        public ICommand UploadCommand
        {
            get { return (ICommand)GetValue(UploadCommandProperty); }
            set { SetValue(UploadCommandProperty, value); }
        }

        #endregion


        public UploadFileWidget()
        {
            InitializeComponent();

            EditPathCommand = new RelayCommand(o => EditPath());
        }

        private void EditPath()
        {
            
        }
    }
}
