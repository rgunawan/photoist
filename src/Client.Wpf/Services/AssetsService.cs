using System;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Client.Wpf.Helpers;

namespace Client.Wpf.Services
{
    /// <summary>
    /// Gets assets from the Photoist service
    /// </summary>
    interface IAssetsService
    {
        /// <summary>
        /// Gets image specified in <paramref name="imageUrl"/>
        /// </summary>
        /// <param name="imageUrl">The URL of the image to get.</param>
        /// <returns><see cref="BitmapImage"/> or null if the asset does not exist.</returns>
        BitmapImage GetImage(string imageUrl);
    }

    class AssetsService : IAssetsService
    {
        private readonly IPhotoistApiServer _server;
        private readonly ILogger _logger;

        public AssetsService(IPhotoistApiServer server, ILogger logger)
        {
            _server = server;
            _logger = logger;
        }

        public BitmapImage GetImage(string imageUrl)
        {
            var imageTask =  _server.SendGetCommandAsync(imageUrl);
            imageTask.Wait();

            var result = imageTask.Result;

            BitmapImage bitmap;

            try
            {
                bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = result.AsStream();
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                bitmap = null;
            }

            return bitmap;
        }
    }
}