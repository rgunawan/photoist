using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Client.Wpf.Helpers;
using Client.Wpf.Properties;

namespace Client.Wpf.Services
{
    interface IPhotoistApiServer
    {
        /// <summary>
        /// Sends HTTP GET command to <paramref name="relativeUri"/>. Returns the raw response.
        /// </summary>
        /// <param name="relativeUri">The relative address for the command. Implementer will supply the base URL.</param>
        /// <returns>The raw response. If there are exceptions, returns bad request response.</returns>
        Task<HttpResponseMessage> SendGetCommandAsync(string relativeUri);

        Task<string> PostFile(string relativeUri, string keyName, string fileName, Stream content);
    }

    class PhotoistApiServer : IPhotoistApiServer
    {
        private readonly ILogger _logger;
        private readonly Uri _apiUrlBase = new Uri(Settings.Default.PhotoistServiceUrl.IsEmpty() ? @"http://localhost:5000/api/" : Settings.Default.PhotoistServiceUrl);

        public PhotoistApiServer(ILogger logger)
        {
            _logger = logger;
        }
        
        public async Task<HttpResponseMessage> SendGetCommandAsync(string relativeUri)
        {
            HttpResponseMessage response;

            var fullUrl = GetFullUri(relativeUri);

            // TODO validate fullUrl
            try
            {
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    response = await client.GetAsync(fullUrl);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            
            return response;
        }

        public async Task<string> PostFile(string relativeUri, string keyName, string fileName,  Stream content)
        {
            string output;
            var fullUrl = GetFullUri(relativeUri);

            // TODO validate fullUrl
            try
            {
                HttpContent fileStreamContent = new StreamContent(content);
                using (var client = new HttpClient())
                using (var formData = new MultipartFormDataContent())
                {
                    formData.Add(fileStreamContent, keyName, fileName);
                    var response = await client.PostAsync(fullUrl, formData);
                    output = response.IsSuccessStatusCode? response.Content.ReadAsStringAsync().Result : string.Empty;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                output = string.Empty;
            }

            return output;
        }

        private Uri GetFullUri(string relativeUri)
        {
            return new Uri(_apiUrlBase, relativeUri);
        }
    }
}