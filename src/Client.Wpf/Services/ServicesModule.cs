﻿using Autofac;
using Client.Wpf.Models;

namespace Client.Wpf.Services
{
    /// <summary>
    /// Autofac module for the Services namespace
    /// </summary>
    class ServicesModule : Module
    {
        #region Overrides of Module

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<PhotosService>().As<IPhotosService>().As<IItemsService<PhotoInfo>>().SingleInstance();
            builder.RegisterType<PhotoistApiServer>().As<IPhotoistApiServer>().SingleInstance();
            builder.RegisterType<AssetsService>().As<IAssetsService>().SingleInstance();
        }

        #endregion
    }
}
