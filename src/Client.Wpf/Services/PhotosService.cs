﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Client.Wpf.Helpers;
using Client.Wpf.Models;
using Client.Wpf.Properties;
using ImageProcessor;
using Newtonsoft.Json;

namespace Client.Wpf.Services
{
    interface IItemsService<T>
    {
        /// <summary>
        /// Gets (eventually paged) <see cref="T"/> from the server.
        /// </summary>
        Task<PaginatedItems<T>> GetItemsAsync(int page);
    }
    /// <summary>
    /// Manage photos from the Photoist service
    /// </summary>
    interface IPhotosService : IItemsService<PhotoInfo>
    {
        /// <summary>
        /// Reads content of <paramref name="filePath"/> and send it to the server.
        /// </summary>
        void PostPhoto(string filePath);
    }

    class PhotosService : IPhotosService
    {
        private readonly IPhotoistApiServer _server;
        private readonly IFileHelpers _fileHelpers;
        private readonly ILogger _logger;
        private readonly IJsonConverter _jsonConverter;

        public PhotosService(IPhotoistApiServer server, IFileHelpers fileHelpers, ILogger logger, IJsonConverter jsonConverter)
        {
            _server = server;
            _fileHelpers = fileHelpers;
            _logger = logger;
            _jsonConverter = jsonConverter;
        }

        #region GetPhotos
        
        /// <summary>
        /// TODO implement <paramref name="page"/>
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<PaginatedItems<PhotoInfo>> GetItemsAsync(int page)
        {
            // TODO implement page

            var result = await _server.SendGetCommandAsync(ApiRelativeUrls.Default.Photos);
            
            // TODO parse get result

            PaginatedItems<PhotoInfo> resultItems;

            try
            {
                resultItems = _jsonConverter.Deserialize<PaginatedItems<PhotoInfo>>(result.AsString());
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                resultItems = new PaginatedItems<PhotoInfo>(new PhotoInfo[0], 0, -1);
            }

            return resultItems;
        }

        #endregion

        #region PostPhoto

        // TODO return result
        public void PostPhoto(string filePath)
        {
            var filename = _fileHelpers.GetFilename(filePath);
            var photoStream = _fileHelpers.ReadAsStream(filePath);
            if (photoStream != null)
            {
                _server.PostFile(ApiRelativeUrls.Default.Photos, "fileToUpload", filename, photoStream);
            }
        }

        #endregion

    }
}
