﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using Client.Wpf.Helpers;

namespace Client.Wpf.ViewModels
{

    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Set the value with <see cref="INotifyPropertyChanged"/> notification.
        /// Sample: 
        /// </summary>
        /// <typeparam name="T">The type of the field</typeparam>
        /// <param name="field">The field to update</param>
        /// <param name="value">The new value</param>
        /// <param name="comparer"><see cref="IEqualityComparer{T}"/> to check if field == value</param>
        /// <param name="propertyName">Property name to use with <see cref="INotifyPropertyChanged"/></param>
        protected virtual void SetValue<T>(ref T field, T value, IEqualityComparer<T> comparer = null, [CallerMemberName] string propertyName = null)
        {
            if (comparer == null)
            {
                comparer = EqualityComparer<T>.Default;
            }

            if (!comparer.Equals(field, value))
            {
                field = value;
                // ReSharper disable once ExplicitCallerInfoArgument
                OnPropertyChanged(propertyName);
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public abstract class NotifyErrorViewModelBase : ViewModelBase, INotifyDataErrorInfo
    {
        #region INotifyDataErrorInfo

        protected readonly IDictionary<string, IEnumerable<string>> Errors = new Dictionary<string, IEnumerable<string>>();

        protected override void SetValue<T>(ref T field, T value, IEqualityComparer<T> comparer = null, [CallerMemberName] string propertyName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            base.SetValue(ref field, value, comparer, propertyName);

            ValidateProperty(propertyName);
        }

        protected virtual void ValidateProperty(string propertyName)
        {
            var prop = GetType().GetProperty(propertyName);
            if (prop != null)
            {
                var value = prop.GetValue(this);
                var validationContext = new ValidationContext(this)
                {
                    MemberName = propertyName
                };

                ICollection<ValidationResult> validationResults = new List<ValidationResult>();
                Validator.TryValidateProperty(value, validationContext, validationResults);
                Errors[propertyName] = validationResults.Select(r => r.ErrorMessage).ToList();
            }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            IEnumerable<string> output;

            if (!Errors.TryGetValue(propertyName, out output))
                output = new string[0];

            return output;
        }

        public bool HasErrors => Errors.Count > 0;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        #endregion
    }


    /// <summary>
    /// This is used only in Startup Wizard, since that's old. Use NotifyErrorViewModelBase instead.
    /// </summary>
    public abstract class ValidatingViewModelBase : ViewModelBase, IDataErrorInfo
    {
        public bool SuspendValidation { get; set; }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (!SuspendValidation)
            {
                RunValidation(propertyName);
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            base.OnPropertyChanged(propertyName); // ignore Resharper since passing propertyName is the correct behavior.
        }

        #region IDataErrorInfo

        private readonly IDictionary<string, string> _errorList = new Dictionary<string, string>();

        public string this[string columnName]
        {
            get
            {
                string output;
                _errorList.TryGetValue(columnName, out output);
                return output;
            }
        }

        private string _error;
        public string Error
        {
            get { return _error; }
            set { SetValue(ref _error, value); }
        }

        private bool _hasErrors;
        public bool HasErrors
        {
            get { return _hasErrors; }
            set { SetValue(ref _hasErrors, value); }
        }

        #endregion

        /// <summary>
        /// Validate a single property. Updates <see cref="IDataErrorInfo.this"/>, <see cref="IDataErrorInfo.Error"/> and <see cref="HasErrors"/>
        /// </summary>
        /// <param name="propertyName"></param>
        public void RunValidation(string propertyName)
        {
            var errorMessage = ValidateProperty(propertyName);

            if (errorMessage.IsEmpty())
            {
                _errorList.Remove(propertyName);
            }
            else
            {
                if (!_errorList.Values.Any(x => x.Contains(errorMessage)))
                    _errorList[propertyName] = errorMessage;
            }

            // Update error properties
            Error = string.Join("\n", _errorList.Values);
            HasErrors = !Error.IsEmpty();
        }

        /// <summary>
        /// Run all validations on all properties. Updates <see cref="IDataErrorInfo.this"/>, <see cref="IDataErrorInfo.Error"/> and <see cref="HasErrors"/>
        /// </summary>
        public virtual bool RunAllValidations()
        {
            var propNames = GetType().GetProperties().Select(p => p.Name);
            foreach (var propName in propNames)
            {
                // OnPropertyChange will validate AND make the control shows the error.
                // ReSharper disable once ExplicitCallerInfoArgument
                OnPropertyChanged(propName); // ignore Resharper since passing propertyName is the correct behavior.
            }

            return !HasErrors;
        }

        /// <summary>
        /// Run all validations on all properties. Does not update any properties
        /// </summary>
        public virtual bool IsAllValid()
        {
            var propNames = GetType().GetProperties().Select(p => p.Name);
            return propNames.All(p => ValidateProperty(p).IsEmpty());
        }

        /// <summary>
        /// Validate a property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>Error message when property is invalid. <see cref="string.Empty"/> or null when property is valid.</returns>
        protected abstract string ValidateProperty(string propertyName);

    }
}
