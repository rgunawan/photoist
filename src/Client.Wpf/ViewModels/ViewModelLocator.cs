﻿namespace Client.Wpf.ViewModels
{
    class ViewModelLocator
    {
        public PhotosViewModel PhotosViewModel { get; }

        public ViewModelLocator()
        {
            
        }

        public ViewModelLocator(PhotosViewModel photosViewModel)
        {
            PhotosViewModel = photosViewModel;
        }
    }
}