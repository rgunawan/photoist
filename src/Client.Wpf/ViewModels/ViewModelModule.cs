﻿using Autofac;
using Client.Wpf.Helpers;
using Client.Wpf.Models;

namespace Client.Wpf.ViewModels
{
    class ViewModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ViewModelLocator>();
            builder.RegisterType<PhotosViewModel>();
            
            builder.RegisterType<Pager<PhotoInfo>>().As<IPager<PhotoInfo>>();
        }
    }
}
