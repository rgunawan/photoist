﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Client.Wpf.Helpers;
using Client.Wpf.Helpers.Xaml;
using Client.Wpf.Models;
using Client.Wpf.Services;

namespace Client.Wpf.ViewModels
{


    class PhotosViewModel : ViewModelBase
    {
        #region Notifying Properties

        private ObservableCollection<PhotoViewModel> _photos;
        public ObservableCollection<PhotoViewModel> Photos
        {
            get { return _photos; }
            set { SetValue(ref _photos, value); }
        }

        private ICommand _uploadFileCommand;
        public ICommand UploadFileCommand
        {
            get { return _uploadFileCommand; }
            set { SetValue(ref _uploadFileCommand, value); }
        }

        private ICommand _refreshCommand;
        public ICommand RefreshCommand
        {
            get { return _refreshCommand; }
            set { SetValue(ref _refreshCommand, value); }
        }

        #endregion

        private readonly IPhotosService _photosService;
        private readonly IAssetsService _assetsService;
        private readonly IFileHelpers _fileHelpers;
        private readonly IDispatcherHelpers _dispatcher;
        private readonly IPager<PhotoInfo> _photoPager;

        public PhotosViewModel(IPager<PhotoInfo> photoPager, IPhotosService photosService, IAssetsService assetsService, IFileHelpers fileHelpers, IDispatcherHelpers dispatcher)
        {
            // TODO use routed command ??
            UploadFileCommand = new RelayCommand(UploadFile, CanUploadFile);
            RefreshCommand = new RelayCommand(o => Refresh());

            _photosService = photosService;
            _assetsService = assetsService;
            _fileHelpers = fileHelpers;
            _dispatcher = dispatcher;

            Photos = new ObservableCollection<PhotoViewModel>();
            
            _photoPager = photoPager;
            _photoPager.ItemsObservable.Subscribe(OnNextPhotoItems);
            _photoPager.Load();
        }

        private void OnNextPhotoItems(IEnumerable<PhotoInfo> photoInfos)
        {
            Task.Run(() =>
            {
                var newPhotos = photoInfos.Select(photoInfo =>
                    {
                        var imageAsset = _assetsService.GetImage(photoInfo.ThumbnailUrl);
                        
                        return new PhotoViewModel { Image = imageAsset };
                    });

                UpdatePhotos(newPhotos);
            });
        }

        private void UpdatePhotos(IEnumerable<PhotoViewModel> newPhotos)
        {
            var localNewPhotos = newPhotos.ToArray(); // make sure we're not running deferred LINQ in the main thread.
            _dispatcher.Invoke(() =>
            {
                Photos = new ObservableCollection<PhotoViewModel>(localNewPhotos);
            });
        }

        #region UploadFileCommand

        private bool CanUploadFile(object pathParam)
        {
            var path = pathParam as string;
            return !path.IsEmpty() && _fileHelpers.FileExists(path);
        }

        private void UploadFile(object pathParam)
        {
            var path = pathParam as string;
            if (path.IsEmpty())
                return;

            _photosService.PostPhoto(path);
        }

        #endregion

        #region RefreshCommand

        private void Refresh()
        {
            _photoPager.Load();
        }

        #endregion


    }

    class PhotoViewModel : ViewModelBase
    {
        #region Notifying Properties

        private BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
            set { SetValue(ref _image, value); }
        }



        #endregion
    }
}
