﻿using System;
using System.IO;
using System.Net.Http;

namespace Client.Wpf.Helpers
{
    static class HttpResponseHelpers
    {
        // support for mocking for unit tests
        internal static Func<HttpResponseMessage, string> AsStringFunc;


        public static string AsString(this HttpResponseMessage response)
        {
            if (AsStringFunc != null)
                return AsStringFunc(response);
            
            string output;

            if (response == null)
                output = string.Empty;
            else
                output = response.IsSuccessStatusCode
                    ? response.Content.ReadAsStringAsync().Result
                    : string.Empty;

            return output;
        }

        public static Stream AsStream(this HttpResponseMessage response)
        {
            Stream output;

            if (response == null)
                output = default(Stream);
            else
                output = response.IsSuccessStatusCode
                ? response.Content.ReadAsStreamAsync().Result
                : default(Stream);

            return output;
        }

        public static byte[] AsBytes(this HttpResponseMessage response)
        {
            byte[] output;

            if (response == null)
                output = new byte[0];
            else
                output = response.IsSuccessStatusCode
                ? response.Content.ReadAsByteArrayAsync().Result
                : new byte[0];

            return output;
        }
    }
}
