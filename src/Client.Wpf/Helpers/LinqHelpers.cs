﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Client.Wpf.Helpers
{

    public static class LinqHelpers
    {
        public static IEnumerable<T> TakeUntil<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            foreach (var item in source)
            {
                var shouldStop = predicate(item);
                yield return item;
                if (shouldStop)
                {
                    yield break;
                }
            }
        }

        public static bool IsEmpty<T>(this IEnumerable<T> source)
        {
            if (source == null)
            {
                return true;
            }

            var stringSource = source as string;
            if (stringSource != null)
            {
                return string.IsNullOrWhiteSpace(stringSource);
            }

            return !source.Any();
        }

        /// <summary>
        /// The opposite of IsEmpty. Added because IsEmpty is often used as !source.IsEmpty(), and that's a double negative.
        /// </summary>
        public static bool HasItems<T>(this IEnumerable<T> source)
        {
            return !IsEmpty(source);
        }
    }
}
