﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Windows.Interop;
//using System.Windows.Media;
//using Microsoft.Win32;

//namespace Client.Wpf.Helpers
//{
//    class CommonDialogHelpers
//    {

//        public static string EditFolderPath(string startingPath, string description = "", Window owner = null)
//        {
//            var output = startingPath;

//            try
//            {
//                using (var folderBrowser = new FolderSelectDialog())
//                {
//                    folderBrowser.Title = description;
//                    folderBrowser.InitialDirectory = startingPath;
//                    if (folderBrowser.ShowDialog())
//                    {
//                        output = folderBrowser.FileName;
//                    }
//                    else
//                        output = string.Empty;
//                }
//            }
//            catch (Exception e)
//            {
//                Logger.TheLogger.Error().WriteLine(e);
//            }

//            return output;
//        }

//        public static string EditSaveFilePath(string startingPath, string extension = "", Window owner = null)
//        {
//            var output = "";

//            var win32Owner = owner == null ? null : owner.GetIWin32Window();

//            using (var fileBrowser = new SaveFileDialog())
//            {
//                try
//                {
//                    fileBrowser.FileName = Path.GetFileName(startingPath);
//                    fileBrowser.InitialDirectory = Path.GetDirectoryName(startingPath);
//                }
//                catch (Exception)
//                {

//                }

//                fileBrowser.Filter = extension;
//                if (fileBrowser.ShowDialog(win32Owner) == DialogResult.OK)
//                {
//                    output = fileBrowser.FileName;
//                }
//            }

//            return output;
//        }

//        #region Win32 window

//        // Taken from http://stackoverflow.com/questions/5622854/how-do-i-show-a-save-as-dialog-in-wpf

//        public static IWin32Window GetIWin32Window(this Visual visual)
//        {
//            var source = PresentationSource.FromVisual(visual) as HwndSource;
//            IWin32Window win = new OldWindow(source.Handle);
//            return win;
//        }

//        private class OldWindow : IWin32Window
//        {
//            private readonly IntPtr _handle;
//            public OldWindow(IntPtr handle)
//            {
//                _handle = handle;
//            }

//            IntPtr IWin32Window.Handle
//            {
//                get { return _handle; }
//            }
//        }

//        #endregion


//        public static string EditOpenFilePath(string startingPath, string extension = "", Window owner = null)
//        {
//            var win32Owner = owner == null ? null : owner.GetIWin32Window();

//            using (var fileBrowser = new OpenFileDialog())
//            {
//                if (!string.IsNullOrEmpty(startingPath))
//                {
//                    try
//                    {
//                        fileBrowser.InitialDirectory = Path.GetDirectoryName(startingPath);
//                    }
//                    catch (Exception ex)
//                    {
//                        App.TheLogger.Warning().WriteLine(ex, "Error getting directory name for path: {0}".Fill(startingPath));
//                        // not a valid path, just don't use it.
//                    }
//                }

//                fileBrowser.FileName = startingPath;
//                fileBrowser.Filter = extension;
//                fileBrowser.CheckFileExists = true;
//                if (fileBrowser.ShowDialog(win32Owner) == DialogResult.OK)
//                {
//                    return fileBrowser.FileName;
//                }
//            }

//            return startingPath;
//        }
//    }
//}
//}
