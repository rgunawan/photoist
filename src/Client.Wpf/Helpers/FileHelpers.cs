﻿using System;
using System.IO;

namespace Client.Wpf.Helpers
{
    interface IFileHelpers
    {
        string GetFilename(string filePath);
        bool FileExists(string path);
        Stream ReadAsStream(string filePath);
    }

    class FileHelpers : IFileHelpers
    {
        private readonly ILogger _logger;

        public FileHelpers(ILogger logger)
        {
            _logger = logger;
        }

        public bool FileExists(string path)
        {
            return TryOrDefault(() => File.Exists(path));
        }

        public Stream ReadAsStream(string filePath)
        {
            return TryOrDefault(() => File.OpenRead(filePath));
        }

        public string GetFilename(string filePath)
        {
            return TryOrDefault(() => Path.GetFileName(filePath));
        }

        private T TryOrDefault<T>(Func<T> action)
        {
            T output;

            try
            {
                output = action();
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                output = default(T);
            }

            return output;
        }
    }
}
