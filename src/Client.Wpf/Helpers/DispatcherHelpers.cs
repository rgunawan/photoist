﻿using System;
using System.Windows;

namespace Client.Wpf.Helpers
{
    class DispatcherHelpers : IDispatcherHelpers
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Unit tests do not need Dispatchers. When set to true, the Dispatcher would not be invoked. Action will just be run.
        /// </summary>
        public static bool IsUnitTest { get; set; }

        public DispatcherHelpers(ILogger logger)
        {
            _logger = logger;
        }

        public void Invoke(Action invokeAction)
        {
            if (Application.Current != null && !Application.Current.Dispatcher.HasShutdownStarted)
            {
                try
                {
                    Application.Current.Dispatcher.Invoke(invokeAction);
                }
                catch (Exception e)
                {
                    _logger.LogError(e);
                }
            }
            else if (IsUnitTest)
            {
                // We're running unit tests, don't worry about dispatchers.
                invokeAction();
            }
        }
    }
}
