﻿using Newtonsoft.Json;

namespace Client.Wpf.Helpers
{
    interface IJsonConverter
    {
        T Deserialize<T>(string input);
    }

    class JsonConverter : IJsonConverter
    {
        public T Deserialize<T>(string input)
        {
            return JsonConvert.DeserializeObject<T>(input);
        }
    }
}
