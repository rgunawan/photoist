﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using Client.Wpf.Models;
using Client.Wpf.Services;

namespace Client.Wpf.Helpers
{
    interface IPager<out T>
    {
        int CurrentPage { get; }
        int TotalPages { get; }
        IObservable<IEnumerable<T>> ItemsObservable { get; }
        void Next();
        void Prev();
        void Goto(int page);

        /// <summary>
        /// Load the current page if we have a current page. Otherwise, load the first page.
        /// </summary>
        void Load();
        
    }

    class Pager<T> : IPager<T>
    {
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public IObservable<IEnumerable<T>> ItemsObservable { get; private set; }

        private readonly BehaviorSubject<IEnumerable<T>> _itemsSource;
        private readonly IItemsService<T> _itemsService;
        private readonly ILogger _logger;

        public Pager(IItemsService<T> itemsService, ILogger logger)
        {
            _itemsService = itemsService;
            _logger = logger;
            CurrentPage = -1;
            _itemsSource = new BehaviorSubject<IEnumerable<T>>(new T[0]);
            ItemsObservable = _itemsSource.AsObservable();
        }

        public async void Next()
        {
            await GotoPage(CurrentPage + 1);
        }

        public async void Prev()
        {
            await GotoPage(CurrentPage - 1);
        }

        public async void Goto(int page)
        {
            await GotoPage(page);
        }

        public async void Load()
        {
            if (CurrentPage < 0)
                await GotoPage(0);
            else
                await GotoPage(CurrentPage);
        }

        private async Task GotoPage(int page)
        {
            try
            {
                var nextItems = await _itemsService.GetItemsAsync(page);
                CurrentPage = nextItems.Page;
                TotalPages = nextItems.TotalPages;
                _itemsSource.OnNext(nextItems.Items);
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                CurrentPage = -1;
                TotalPages = 0;
                _itemsSource.OnNext(new T[0]);
            }
        }
    }
}
