﻿using System;
using System.Runtime.CompilerServices;

namespace Client.Wpf.Helpers
{
    public enum LogLevel
    {
        Error,
        Warning,
        Info,
        Debug
    }

    interface ILogger
    {
        void LogInfo (string message, [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0);
        void LogDebug(string message, [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0);
        void LogError(Exception e, string message = null, [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0);
        void LogWarning(string message, [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int lineNumber = 0);
        LogLevel LogLevel { get; set; }
    }

    class TodoLogger : ILogger
    {
        public LogLevel LogLevel { get; set; }

        public void LogInfo(string message, string callerFilePath = "", string callerMemberName = "", int lineNumber = 0)
        {
            // TODO implement logging
        }

        public void LogDebug(string message, string callerFilePath = "", string callerMemberName = "", int lineNumber = 0)
        {
            // TODO implement logging
        }

        public void LogError(Exception e, string message = null, string callerFilePath = "", string callerMemberName = "", int lineNumber = 0)
        {
            // TODO implement logging
        }

        public void LogWarning(string message, string callerFilePath = "", string callerMemberName = "", int lineNumber = 0)
        {
            // TODO implement logging
        }
    }
}