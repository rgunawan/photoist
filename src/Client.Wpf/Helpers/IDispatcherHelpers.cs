﻿using System;

namespace Client.Wpf.Helpers
{
    interface IDispatcherHelpers
    {
        void Invoke(Action invokeAction);
    }
}