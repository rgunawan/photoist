﻿using Autofac;

namespace Client.Wpf.Helpers
{
    class HelpersModule : Module
    {
        #region Overrides of Module

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<TodoLogger>().As<ILogger>().SingleInstance();
            builder.RegisterType<FileHelpers>().As<IFileHelpers>().SingleInstance();
            builder.RegisterType<DispatcherHelpers>().As<IDispatcherHelpers>().SingleInstance();
            builder.RegisterType<JsonConverter>().As<IJsonConverter>().SingleInstance();
        }

        #endregion
    }
}