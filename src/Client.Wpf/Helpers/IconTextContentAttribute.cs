﻿using System;
using System.Globalization;
using Client.Wpf.Controls;

namespace Client.Wpf.Helpers
{
    class IconTextContentAttribute : Attribute
    {
        private char CharContent { get; }

        public IconTextContentAttribute(int charContent)
        {
            CharContent = (char) charContent;
        }

        public static char GetContentText(IconBlock.IconBlockName name)
        {
            var info =  typeof(IconBlock.IconBlockName).GetField(Enum.GetName(typeof(IconBlock.IconBlockName), name));
            var attr = (IconTextContentAttribute) GetCustomAttribute(info, typeof(IconTextContentAttribute));

            return attr.CharContent;
        }
    }
}
