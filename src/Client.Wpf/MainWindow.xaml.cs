﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowsCommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // window buttons
            if (e.Command == SystemCommands.CloseWindowCommand)
                e.CanExecute = true; 
            else if (e.Command == SystemCommands.MaximizeWindowCommand)
                e.CanExecute = WindowState != WindowState.Maximized;
            else if (e.Command == SystemCommands.MinimizeWindowCommand)
                e.CanExecute = WindowState != WindowState.Minimized;
            else if (e.Command == SystemCommands.RestoreWindowCommand)
                e.CanExecute = WindowState != WindowState.Normal;
        }

        private void WindowsCommandExecute(object sender, ExecutedRoutedEventArgs e)
        {
            // window buttons
            if (e.Command == SystemCommands.CloseWindowCommand)
                Close();
            else if (e.Command == SystemCommands.MaximizeWindowCommand)
                WindowState = WindowState.Maximized;
            else if (e.Command == SystemCommands.MinimizeWindowCommand)
                WindowState = WindowState.Minimized;
            else if (e.Command == SystemCommands.RestoreWindowCommand)
                WindowState = WindowState.Normal;
        }

        private void OnMastheadLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // drag masthead => drag window
            DragMove();
        }

        private void OnMastheadDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // maximize/restore window
            if (e.ChangedButton == MouseButton.Left)
            {
                if (WindowState == WindowState.Normal)
                    WindowState = WindowState.Maximized;
                else if (WindowState == WindowState.Maximized)
                    WindowState = WindowState.Normal;
            }
        }
    }
}
