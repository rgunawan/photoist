import { PhotosComponent } from './pages/photos/photos.component';
import { RouterModule } from '@angular/router';

export class AppRouting {
    public static routes : RouterModule[] = [
      { path: 'photos', component: PhotosComponent },
      // { path: 'crisis-center', component: CrisisListComponent },
      // {
      //   path: 'heroes',
      //   component: HeroListComponent,
      //   data: {
      //     title: 'Heroes List'
      //   }
      // },
      { path: '', component: PhotosComponent },
      // { path: '**', component: PageNotFoundComponent }
    ]
}