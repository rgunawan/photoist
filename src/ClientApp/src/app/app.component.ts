import { Component } from '@angular/core';

import { MastheadComponent } from './shared/controls/masthead.component';
import { NavItem } from './shared/controls/navItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Photoist';
  private _navItems: NavItem[];
  public get navItems(): NavItem[] {
    return this._navItems;
  }
  public set navItems(v: NavItem[]) {
    this._navItems = v;
  }

  /**
   *
   */
  constructor() {

    this._navItems = [
      new NavItem("Photos", "photos"),
      // new NavItem("hello1", "world1"),
      // new NavItem("hello2", "world2")
    ];
  }
}
