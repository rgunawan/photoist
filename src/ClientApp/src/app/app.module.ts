import { PhotosService } from './services/photos.service';
import { AppRouting } from './app.routing';
import { ControlsModule } from './shared/controls.module';
import { PhotosComponent } from './pages/photos/photos.component';
import { PhotosModule } from "./pages/photos/photos.module";

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule,
    RouterModule.forRoot(AppRouting.routes),  // AppRouting.routes sets up routing
    ControlsModule, PhotosModule
  ],
  providers: [PhotosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
