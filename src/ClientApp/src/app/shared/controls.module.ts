import { NavbarComponent } from './controls/navbar.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MastheadComponent } from './controls/masthead.component';

@NgModule({
    imports: [CommonModule],
    exports: [MastheadComponent, NavbarComponent],
    declarations: [MastheadComponent, NavbarComponent],
    providers: [],
})
export class ControlsModule { }
