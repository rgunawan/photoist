import { Component, Input, OnInit } from '@angular/core';

import { NavbarComponent } from './navbar.component';
import { NavItem } from './navItem';

@Component({
    selector: 'pi-masthead',
    templateUrl: './masthead.component.html',
  
})
export class MastheadComponent implements OnInit {
    @Input() public appName: string;

    private _navItems: NavItem[];
    public get navItems(): NavItem[] {
        return this._navItems;
    }
    @Input() public set navItems(v: NavItem[]) {
        this._navItems = v;
    }


    constructor() {
        // this._navItems = [
        //     new NavItem("hello", "world"),
        //     new NavItem("hello1", "world1"),
        //     new NavItem("hello2", "world2")
        // ];
    }

    ngOnInit() { }
}

