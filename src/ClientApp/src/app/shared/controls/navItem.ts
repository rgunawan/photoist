export  /**
 * NavItem
 */
    class NavItem {

    private _text: string;
    public get text(): string {
        return this._text;
    }
    public set text(v: string) {
        this._text = v;
    }


    private _link: string;
    public get link(): string {
        return this._link;
    }
    public set link(v: string) {
        this._link = v;
    }


    constructor(text: string, link: string) {
        this._text = text;
        this._link = link;
    }
}