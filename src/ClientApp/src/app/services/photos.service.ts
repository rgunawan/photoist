import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import "../shared/helpers/rxjs-operators";

@Injectable()
export class PhotosService {

    private mockServiceResult: any = {
        "items": [
            { "id": 1, "fullSizeUrl": "/api/assets/full/1", "thumbnailUrl": "/api/assets/thumb/1", "metadata": null },
            { "id": 2, "fullSizeUrl": "/api/assets/full/2", "thumbnailUrl": "/api/assets/thumb/2", "metadata": null },
            { "id": 3, "fullSizeUrl": "/api/assets/full/3", "thumbnailUrl": "/api/assets/thumb/3", "metadata": null }],
        "totalPages": 0,
        "page": 0,
        "links": null
    }

    constructor(private http: Http) { }

    public getPhotos(page: number = 0): Observable<PaginatedItems<PhotoInfo>> {
        return this.http
            .get(`api/photos`)
            .map(value => this.extractData(value));
    }

    // using fat arrow so that "this" refers to this class.
    private extractData(value: Response): PaginatedItems<PhotoInfo> {

        let data = value.json();

        let output = new PaginatedItems<PhotoInfo>();
        output.items = data.items;
        output.page = data.page;
        output.totalPages = data.totalPages;

        return output;
    }
}

export class PhotoInfo {
    public readonly id: number;
    public readonly fullSizeUrl: string;
    public readonly thumbnailUrl: string;
    public readonly metadata: any;
}

export class PaginatedItems<T> {
    public items: Array<T>;
    public totalPages: number;
    public page: number;
}