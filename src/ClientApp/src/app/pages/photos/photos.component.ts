import { PhotosService, PaginatedItems, PhotoInfo } from './../../services/photos.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'pi-photos',
    // templateUrl: 'photos.component.html'
    template: `
    <div class="mx-1 my-1">
    <h1>Photos</h1>
        <pi-photo-list [photos]="photos"></pi-photo-list>
    
</div>
`
})
export class PhotosComponent implements OnInit {
    public photos: PaginatedItems<PhotoInfo> = { 
        totalPages: 0,
        page: 0,
        items: []
    };

    constructor(private photosService: PhotosService) { }

    ngOnInit() {
        var subs = this.photosService.getPhotos()
            .subscribe(photos => this.photos = photos);
    }
}