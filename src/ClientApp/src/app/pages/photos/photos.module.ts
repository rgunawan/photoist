import { PhotoListComponent } from './photo-list.component';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PhotosComponent }   from './photos.component';

@NgModule({
    imports: [CommonModule],
    exports: [],
    declarations: [PhotosComponent, PhotoListComponent],
    providers: [],
})
export class PhotosModule { }
