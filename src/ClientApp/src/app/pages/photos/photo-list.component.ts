import { PaginatedItems, PhotoInfo } from './../../services/photos.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pi-photo-list',
    template: `
    
    <div class="card-group">
        <li class="card" *ngFor="let photo of photos.items" (click)="onItemClick(photo)"><img src="{{photo.thumbnailUrl}}"></li>
    </div>
    `
    // templateUrl: 'photo-list.component.html'
})
export class PhotoListComponent implements OnInit {
    @Input() photos: PaginatedItems<PhotoInfo>

    constructor() { }

    ngOnInit() { }

    public onItemClick(clickedItem: PhotoInfo) {
        alert(`hello ${clickedItem.id}`);
    }
}