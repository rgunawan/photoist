﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Common;
using Photoist.Data;
using Photoist.Filters;
using Photoist.Models;
using Photoist.Models.PhotosModels;

namespace Photoist.Controllers
{
    public class PhotosController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public PhotosController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public Photos Index(int id = -1)
        {
            var filteredData = id > -1
                        ? _dbContext.Photos.Where(db => db.Id == id)
                        : _dbContext.Photos;

            // TODO don't load the asset's data.
            var outputData = filteredData
                .Select(db => new Photo
                {
                    Id = db.Id,
                    FullSizeUrl = Url.RouteUrl(new { Controller = "assets", db.Asset.Id, Action = "full" }),
                    ThumbnailUrl = Url.RouteUrl(new { Controller = "assets", db.Asset.Id, Action = "thumb" }),
                }).ToArray();

            return new Photos
            {
                Page = 0,
                TotalPages = 0,
                Items = outputData
            };
        }

        public static byte[] ToByteArray(Stream stream)
        {
            stream.Position = 0;
            byte[] buffer = new byte[stream.Length];
            for (int totalBytesCopied = 0; totalBytesCopied < stream.Length;)
                totalBytesCopied += stream.Read(buffer, totalBytesCopied,
                    Convert.ToInt32(stream.Length) - totalBytesCopied);
            return buffer;
        }

        [RequestFormSizeLimit(valueCountLimit: 3, Order = 1)]
        [HttpPost]
        public void Index()
        {
            // TODO return new ID or failure code
            // save the files uploaded.
            var files = Request.Form.Files;
            var file = files.FirstOrDefault();
            
            if (file != null)
            {
                AssetDbModel fileAsset = null;
                using (var byteStream = new MemoryStream())
                {
                    file.CopyTo(byteStream);
                    var fileBytes = ToByteArray(byteStream);
                    fileAsset = new AssetDbModel
                    {
                        Data = fileBytes
                    };
                    _dbContext.Assets.Add(fileAsset);
                }

                // save the photo.
                var newPhoto = new PhotoDbModel
                {
                    Metadata = new PhotoMetadataDbModel(),
                    Asset = fileAsset
                };

                _dbContext.Photos.Add(newPhoto);

                _dbContext.SaveChanges();

            }
        }
    }
}