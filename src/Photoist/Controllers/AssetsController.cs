using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ImageProcessorCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Photoist.Data;

namespace Photoist.Controllers
{
    public class AssetsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<AssetsController> _logger;

        private static IDictionary<int, Image<Color, uint>> _imageCache = new Dictionary<int, Image<Color, uint>>();

        public AssetsController(ApplicationDbContext dbContext, ILogger<AssetsController> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet]
        [ActionName("full")]
        public async Task<FileStreamResult> GetFull(int id)
        {
            var fileBytes = await GetAssetBytes(id);
            var ms = new MemoryStream(fileBytes);
            return new FileStreamResult(ms, "image/jpeg");
        }

        [HttpGet]
        [ActionName("thumb")]
        public async Task<FileStreamResult> GetThumbnail(int id, string size)
        {
            FileStreamResult output;

            var msOutput = new MemoryStream();

            if (_imageCache.ContainsKey(id))
            {
                var image = _imageCache[id];
                image.SaveAsJpeg(msOutput);
                _logger.LogDebug("_imageCache hit!");
            }
            else
            {
                var fileBytes = await GetAssetBytes(id);
                using (var msInput = new MemoryStream(fileBytes))
                {
                    //return new FileStreamResult(ms, "image/jpeg");
                    var rawImage = new Image<Color, uint>(msInput);

                    var image = rawImage.AutoOrient();

                    var imageRatio = image.Width > image.Height
                        ? 128.0 / image.Width
                        : 128.0 / image.Height;

                    var newWidth = (int)(image.Width * imageRatio);
                    var newHeight = (int)(image.Height * imageRatio);


                    var outputImage = image.Resize(newWidth, newHeight);
                    outputImage.SaveAsJpeg(msOutput);

                    _imageCache[id] = outputImage;
                    _logger.LogDebug("_imageCache miss!");
                }
            }

            msOutput.Position = 0;
            output = new FileStreamResult(msOutput, "image/jpeg");

            return output;
        }

        private async Task<byte[]> GetAssetBytes(int assetId)
        {
            var file = await _dbContext.Assets.FirstOrDefaultAsync(asset => asset.Id == assetId);
            var output = file?.Data ?? new byte[0];

            return output;
        }
    }
}