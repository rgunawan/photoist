﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using NuGet.Common;

namespace Photoist.Models.PhotosModels
{
    public class Photos : PaginatedResult
    {
        public IEnumerable<Photo> Items { get; set; }

    }

    public class Photo
    {
        public int Id { get; set; }
        public string FullSizeUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public PhotoMetadata Metadata { get; set; }
    }

    public class PhotoMetadata
    {
        public string Text { get; set; }
    }
}
