﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Photoist.Models.PhotosModels
{
    public class PhotoDbModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public PhotoMetadataDbModel Metadata { get; set; }
        public AssetDbModel Asset { get; set; }
    }

    public class PhotoMetadataDbModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
    }
}