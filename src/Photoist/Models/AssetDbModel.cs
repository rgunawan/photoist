﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Photoist.Models
{
    public class AssetDbModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public byte[] Data { get; set; }
    }
}
