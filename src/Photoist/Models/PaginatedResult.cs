﻿using System.Collections.Generic;

namespace Photoist.Models
{
    public class Result
    {
        public IEnumerable<string> Links { get; set; }
    }

    public class PaginatedResult : Result
    {
        public long TotalPages { get; set; }
        public long Page { get; set; }
    }
}
