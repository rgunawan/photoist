﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Client.Wpf.Helpers;
using Client.Wpf.Models;
using Client.Wpf.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace Client.Wpf.Test.Services
{
    [TestClass]
    public class PhotosServiceTest
    {

        [TestMethod]
        public void GetPhotosShouldGetFromServerAndReturnItems()
        {
            PaginatedItems<PhotoInfo> mockResult;
            Mock<ILogger> mockLogger;
            Mock<IFileHelpers> mockFileHelpers;
            Mock<IPhotoistApiServer> mockPhotoistApiServer;
            Mock<IJsonConverter> mockJsonConverter;

            var target = GetTarget(out mockResult, out mockPhotoistApiServer, out mockFileHelpers, out mockLogger, out mockJsonConverter);

            var resultTask = target.GetItemsAsync(0);
            resultTask.Wait();
            var result = resultTask.Result;

            Assert.IsNull(resultTask.Exception, "exception is not null");
            Assert.IsNotNull(result, "result is null");
            Assert.AreSame(mockResult, result);

            // verify that an http command went out.
            mockPhotoistApiServer.VerifyAll();
        }

        [TestMethod]
        public void GetPhotosJsonExceptionsReturnsEmptyItems()
        {
            PaginatedItems<PhotoInfo> mockResult;
            Mock<ILogger> mockLogger;
            Mock<IFileHelpers> mockFileHelpers;
            Mock<IPhotoistApiServer> mockPhotoistApiServer;
            Mock<IJsonConverter> mockJsonConverter;

            var target = GetTarget(out mockResult, out mockPhotoistApiServer, out mockFileHelpers, out mockLogger, out mockJsonConverter);

            var mockResultAsString = JsonConvert.SerializeObject(mockResult);
            mockJsonConverter.Setup(converter => converter.Deserialize<PaginatedItems<PhotoInfo>>(mockResultAsString))
                .Throws<Exception>();

            var resultTask = target.GetItemsAsync(0);
            resultTask.Wait();
            var result = resultTask.Result;

            Assert.IsNull(resultTask.Exception, "exception is not null");
            Assert.IsNotNull(result, "result is null");
            Assert.AreEqual(0, result.TotalPages);
            Assert.AreEqual(-1, result.Page);
            Assert.AreEqual(0, result.Items.Count());

            // verify that an http command went out.
            mockPhotoistApiServer.VerifyAll();
        }

        [TestMethod]
        public void GetPhotosAsStringExceptionsReturnsEmptyItems()
        {
            PaginatedItems<PhotoInfo> mockResult;
            Mock<ILogger> mockLogger;
            Mock<IFileHelpers> mockFileHelpers;
            Mock<IPhotoistApiServer> mockPhotoistApiServer;
            Mock<IJsonConverter> mockJsonConverter;

            Func<HttpResponseMessage, string> mockAsStringFunc = message => { throw new Exception(); };

            var target = GetTarget(out mockResult, out mockPhotoistApiServer, out mockFileHelpers, out mockLogger, out mockJsonConverter,
                mockAsStringFunc: mockAsStringFunc);

            var resultTask = target.GetItemsAsync(0);
            resultTask.Wait();
            var result = resultTask.Result;

            Assert.IsNull(resultTask.Exception, "exception is not null");
            Assert.IsNotNull(result, "result is null");
            Assert.AreEqual(0, result.TotalPages);
            Assert.AreEqual(-1, result.Page);
            Assert.AreEqual(0, result.Items.Count());

            // verify that an http command went out.
            mockPhotoistApiServer.VerifyAll();
        }

        private IPhotosService GetTarget(out PaginatedItems<PhotoInfo> mockResult, 
            out Mock<IPhotoistApiServer> mockServer, out Mock<IFileHelpers> mockFileHelpers, out Mock<ILogger> mockLogger, out Mock<IJsonConverter> mockJsonConverter,
            Action<Mock<IPhotoistApiServer>> setupServer = null, Func<HttpResponseMessage, string>  mockAsStringFunc = null
            )
        {
            mockLogger = new Mock<ILogger>();
            mockFileHelpers = new Mock<IFileHelpers>();
            mockServer = new Mock<IPhotoistApiServer>(MockBehavior.Strict);
            mockJsonConverter = new Mock<IJsonConverter>();

            mockResult = new PaginatedItems<PhotoInfo>(
                   new[] { new PhotoInfo(1, "fullUrl", "thumbUrl") }
                   , 1, 0);
            var mockResultAsString = JsonConvert.SerializeObject(mockResult);
            // apparently we use a static function, mock the static function.
            mockAsStringFunc = mockAsStringFunc ?? (message => mockResultAsString);
            HttpResponseHelpers.AsStringFunc = mockAsStringFunc;

            setupServer = setupServer ?? (mock => mock.Setup(server => server.SendGetCommandAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpResponseMessage()))
                .Verifiable());

            setupServer(mockServer);

            mockJsonConverter.Setup(converter => converter.Deserialize<PaginatedItems<PhotoInfo>>(mockResultAsString))
                .Returns(mockResult);

            return new PhotosService(mockServer.Object, mockFileHelpers.Object, mockLogger.Object, mockJsonConverter.Object);
        }

        [TestMethod]
        public void PostPhotoShouldSendPhotoToServer()
        {
            PaginatedItems<PhotoInfo> mockResult;
            Mock<ILogger> mockLogger;
            Mock<IFileHelpers> mockFileHelpers;
            Mock<IPhotoistApiServer> mockPhotoistApiServer;
            Mock<IJsonConverter> mockJsonConverter;

            var mockFilePath = "existing file";
            Action<Mock<IPhotoistApiServer>> setupServer = mock =>
            {
                mock.Setup(server => server.PostFile(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Stream>()))
                    .Returns(Task.FromResult(string.Empty))
                    .Verifiable();
            };

            var target = GetTarget(out mockResult, out mockPhotoistApiServer, out mockFileHelpers, out mockLogger, out mockJsonConverter,
                setupServer: setupServer);

            var mockStream = new Mock<Stream>();
            mockFileHelpers.Setup(helpers => helpers.GetFilename(mockFilePath))
                .Returns(mockFilePath);
            // make sure the file is read
            mockFileHelpers.Setup(helpers => helpers.ReadAsStream(mockFilePath))
                .Returns(mockStream.Object)
                .Verifiable();


            target.PostPhoto(mockFilePath);
            
            // verify that an http command went out.
            mockPhotoistApiServer.VerifyAll();
            mockFileHelpers.VerifyAll();
        }

        [TestMethod]
        public void PosPhotoDoNotSendNonExistingFile()
        {
            PaginatedItems<PhotoInfo> mockResult;
            Mock<ILogger> mockLogger;
            Mock<IFileHelpers> mockFileHelpers;
            Mock<IPhotoistApiServer> mockPhotoistApiServer;
            Mock<IJsonConverter> mockJsonConverter;
            
            var mockFilePath = "existing file";

            // if file does not exist, FileHelpers.ReadAsStream returns null
            // and PostFile should not be called.
            // Mock<IPhotoistApiServer> is set to have strict behavior
            // which means that if there is a call to it, the test will fail.

            var target = GetTarget(out mockResult, out mockPhotoistApiServer, out mockFileHelpers, out mockLogger, out mockJsonConverter,
                setupServer: mock => { });

            
            mockFileHelpers.Setup(helpers => helpers.GetFilename(mockFilePath))
                .Returns(mockFilePath);
            // make sure the file is read, and is empty.
            mockFileHelpers.Setup(helpers => helpers.ReadAsStream(mockFilePath))
                .Returns((Stream) null)
                .Verifiable();
            
            target.PostPhoto(mockFilePath);

            // verify that an http command went out.
            mockPhotoistApiServer.VerifyAll();
            mockFileHelpers.VerifyAll();
        }
    }
}
